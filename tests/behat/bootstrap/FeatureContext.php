<?php

namespace App\Tests\Behat;

use Doctrine\ORM\Tools\SchemaTool;

class FeatureContext extends AbstractContext
{

    /**
     * @var \Doctrine\Bundle\DoctrineBundle\Registry|object
     */
    private $doctrine;

    /**
     * @BeforeSuite
     */
    public static function beforeSuite()
    {
        $resultCreateDatabase = exec('php bin/console doctrine:database:create --env=test');
        if (preg_match('/^SQLSTATE/', $resultCreateDatabase)) {
            exec('php bin/console doctrine:schema:drop --env=test --force');
        }
        exec('php bin/console doctrine:schema:create --env=test');
    }

    /**
     * @return \Doctrine\Bundle\DoctrineBundle\Registry|object
     */
    public function getDoctrine()
    {
        if (!$this->doctrine) {
            $this->doctrine = $this->kernel->getContainer()->get('doctrine');
        }

        return $this->doctrine;
    }

    /**
     * @return SchemaTool
     */
    public function getSchemaTool()
    {
        $manager = $this->getDoctrine()->getManager();
        $schemaTool = new SchemaTool($manager);

        return $schemaTool;
    }

    /**
     * @BeforeScenario @createSchema
     */
    public function createSchema()
    {
        $classes = $this->getDoctrine()->getManager()->getMetadataFactory()->getAllMetadata();

        $this->getSchemaTool()->createSchema($classes);
    }

    /**
     * @AfterScenario @dropSchema
     */
    public function dropSchema()
    {
        $classes = $this->getDoctrine()->getManager()->getMetadataFactory()->getAllMetadata();

        $this->getSchemaTool()->dropSchema($classes);
    }

}
