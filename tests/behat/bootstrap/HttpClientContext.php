<?php

namespace App\Tests\Behat;

use Behat\Gherkin\Node\PyStringNode;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HttpClientContext extends AbstractContext
{
    use JsonContext;
    use DoctrineAndFixturesContext;

    /**
     * @var Response|null
     */
    private $response;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @When I send a :method request to :path
     *
     * @param string $method
     * @param string $path
     * @param null|string $body
     */
    public function iSendARequestTo(string $method, string $path, $body = null): void
    {
        $request = Request::create($path, $method, [], [], [], [], $body);
        $request->headers->set('Content-Type', 'application/json');
        $request->headers->set('Accept', 'application/json');

        $this->response = $this->kernel->handle($request);
    }

    /**
     * @Given I send a :method request to :url with body:
     *
     * @param $method
     * @param $url
     * @param PyStringNode $body
     */
    public function iSendARequestToWithBody($method, $url, PyStringNode $body)
    {
        $this->iSendARequestTo($method, $url, $body);
    }

    /**
     * @Then the response status code should be :statusCode
     *
     * @param string $statusCode
     */
    public function theResponseStatusCodeShouldBe(string $statusCode): void
    {
        assertEquals(intval($statusCode), $this->response->getStatusCode(),
            "The status code doesn't egal '$statusCode'"
        );
    }

    /**
     * @Then each :composedKey equals to the fixtures :expected
     *
     * @param $composedKey
     * @param $expected
     */
    public function eachEqualsToTheFixtures($composedKey, $expected): void
    {
        $this->itsValueShouldBeACollection();
        $originalValue = $this->value;

        foreach ($originalValue as $element) {
            $expected = current($this->fixtures)->getTitle();
            $this->theJsonShouldHasKey($composedKey, $element);
            assertEquals($expected, $this->value);
            next($this->fixtures);
        }
        $this->value = $originalValue;
    }
}
