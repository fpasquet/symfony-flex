<?php

namespace App\Tests\Behat;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DemoFeatureContext
 * @package App\Tests\Behat
 */
class DemoFeatureContext extends AbstractContext
{
    /**
     * @var Response|null
     */
    private $response;

    /**
     * @When a demo scenario sends a request to :path
     *
     * @param string $path
     */
    public function aDemoScenarioSendsARequestTo(string $path)
    {
        $this->response = $this->kernel->handle(Request::create($path, 'GET'));
    }

    /**
     * @Then the response should be received
     */
    public function theResponseShouldBeReceived()
    {
        if ($this->response === null) {
            throw new \RuntimeException('No response received');
        }
    }
}
