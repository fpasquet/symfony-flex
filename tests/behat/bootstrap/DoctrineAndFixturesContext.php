<?php

namespace App\Tests\Behat;

trait DoctrineAndFixturesContext
{
    /**
     * @var \Doctrine\Bundle\DoctrineBundle\Registry|object
     */
    private $doctrine;

    /**
     * @var array
     */
    protected $fixtures;

    /**
     * @BeforeScenario
     */
    public static function beforeSuite()
    {
        $resultCreateDatabase = exec('php bin/console doctrine:database:create --env=test');
        if (preg_match('/^SQLSTATE/', $resultCreateDatabase)) {
            exec('php bin/console doctrine:schema:drop --env=test --force');
        }
        exec('php bin/console doctrine:schema:create --env=test');
    }

    /**
     * @return \Doctrine\Bundle\DoctrineBundle\Registry|object
     */
    public function getDoctrine()
    {
        if (!$this->doctrine) {
            $this->doctrine = $this->kernel->getContainer()->get('doctrine');
        }

        return $this->doctrine;
    }

    /**
     * @Given I load fixtures from class :class
     *
     * @param $class
     */
    public function iLoadFixturesFromClass($class)
    {
        $classFixture = new $class();
        $classFixture->load($this->getDoctrine()->getManager());
        $this->fixtures = $classFixture::getData();
    }
}