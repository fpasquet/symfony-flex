<?php

namespace App\Tests\Behat;

use Symfony\Component\HttpFoundation\Response;

trait JsonContext
{
    /**
     * @var Response|null
     */
    private $response;

    /**
     * @var array
     */
    private $responseContent;

    /**
     * @Then the response should be encoded in JSON
     */
    public function theResponseShouldBeEncodedInJson(): void
    {
        $contentType = 'application/json';
        assertContains($contentType, $this->response->headers->get('Content-Type'),
            "The header '$contentType' doesn't contain '$contentType'");

        $this->responseContent = json_decode($this->response->getContent(), true);
    }

    /**
     * @Then its value is same as :value
     *
     * @param $value
     */
    public function itsValueIsSameAs($value): void
    {
        assertEquals(json_encode($value), json_encode($this->value));
    }

    /**
     * @Then its value is null
     */
    public function itsValueIsNull(): void
    {
        assertNull($this->value);
    }

    /**
     * @Then its value should be greater than :value
     *
     * @param $value
     */
    public function itsValueShouldBeGreaterThan($value): void
    {
        $this->itsValueShouldBeAnInteger();
        assertGreaterThan(intval($value), intval($this->value));
    }

    /**
     * @Then its value should be an Integer
     */
    public function itsValueShouldBeAnInteger(): void
    {
        assertTrue(is_int($this->value), sprintf('Value is not an Integer (%s)', gettype($this->value)));
    }

    /**
     * @Then the JSON should has key :composed_key
     *
     * @param $composed_key
     * @param null $var
     */
    public function theJsonShouldHasKey($composed_key, $var = null): void
    {
        $re = '/(?\'key\'\w+)\.?/i';
        preg_match_all($re, $composed_key, $matches, PREG_SET_ORDER, 0);

        if (is_null($var)) {
            $var = $this->responseContent;
        }

        $i = 0;
        do {
            $key = $matches[$i]['key'];
            assertArrayHasKey($key, $var);
            $var = $var[$key];
            $i++;
        } while ($i < count($matches));

        $this->value = $var;
    }

    /**
     * @Then its value should be a collection
     */
    public function itsValueShouldBeACollection(): void
    {
        assertTrue(is_array($this->value), sprintf('Value is not a collection (%s)', gettype($this->value)));
    }

    /**
     * @Then each :composedKey equals to :expected
     *
     * @param $composedKey
     * @param $expected
     */
    public function eachEqualsTo($composedKey, $expected): void
    {
        $this->itsValueShouldBeACollection();
        $originalValue = $this->value;
        foreach ($originalValue as $element) {
            $this->theJsonShouldHasKey($composedKey, $element);
            assertEquals($expected, $this->value);
        }
        $this->value = $originalValue;
    }
}