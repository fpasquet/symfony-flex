Feature: Articles
  In order to use the application
  I need to be able to create and update articles trough the API.

  Scenario: Retrieve articles
    Given I load fixtures from class "App\DataFixtures\ORM\ArticleFixtures"
    When I send a "GET" request to "/api/articles"
    Then the response status code should be 200
    And the response should be encoded in JSON
    And the JSON should has key "data"
    And its value should be a collection
    And each "title" equals to the fixtures "title"

  Scenario: Create a Article OK
    Given I load fixtures from class "App\DataFixtures\ORM\ArticleFixtures"
    When I send a "POST" request to "/api/articles" with body:
    """
      {"title": "Hello world !"}
    """
    Then the response status code should be 200
    And the response should be encoded in JSON
    And the JSON should has key "data.title"
    And its value is same as "Hello world !"