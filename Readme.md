# Symfony 3.3.10

## Notes

  - Docker https://github.com/dunglas/symfony-docker
  - Installer Symfony 3.3.10 avec Flex `composer create-project symfony/skeleton my_project`
  - Installer et activer les annotation `docker-compose exec app composer req annotations`
  - Installer et activer la toolbar de debug `docker-compose exec app composer req profiler`
  - Installer et activer twig `docker-compose exec app composer req template`
  - Installer et activer doctrine `docker-compose exec app composer req doctrine`