<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class Article
 * @package App\Entity
 */
class Article
{
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $title
     */
    private $title;

    /**
     * @var ArrayCollection
     */
    private $tags;

    /**
     * @var \DateTime $createdAt
     */
    private $createdAt;

    /**
     * @var \DateTime $created
     */
    private $updatedAt;

    /**
     * Article constructor.
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): Article
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags(): ArrayCollection
    {
        return $this->tags;
    }

    /**
     * @param Tag $tag
     * @return Article
     */
    public function setTag(Tag $tag): Article
    {
        $this->tags->add($tag);
        return $this;
    }

    /**
     * @param LifecycleEventArgs $event
     * @return $this
     */
    public function setCreatedAtValue(LifecycleEventArgs $event): Article
    {
        $this->createdAt = new \DateTime();
        return $this;
    }

    /**
     * @param LifecycleEventArgs $event
     * @return $this
     */
    public function setUpdatedAtValue(LifecycleEventArgs $event): Article
    {
        $this->updatedAt = new \DateTime();
        return $this;
    }
}