<?php

namespace App\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ArticleFixture
 * @package App\DataFixtures\ORM
 */
class ArticleFixture extends AbstractFixture implements OrderedFixtureInterface
{
    use TraitFixture;

    /**
     * @return string
     */
    public static function getFileNameFixture(): string
    {
        return 'Article.yaml';
    }

    public function load(ObjectManager $manager): void
    {
        foreach (self::getData() as $article) {
            $article->setTag($this->getReference('tag_' . rand(1, 5)));
            $manager->persist($article);
        }

        $manager->flush();
        $manager->clear();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 2;
    }
}