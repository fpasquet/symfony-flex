<?php

namespace App\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

/**
 * Class TagFixture
 * @package App\DataFixtures\ORM
 */
class TagFixture extends AbstractFixture implements OrderedFixtureInterface
{
    use TraitFixture;

    /**
     * @return string
     */
    public static function getFileNameFixture(): string
    {
        return 'Tag.yaml';
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 1;
    }
}