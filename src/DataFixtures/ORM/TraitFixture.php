<?php

namespace App\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Loader\NativeLoader;


trait TraitFixture
{
    /**
     * @var array
     */
    protected static $data = [];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        foreach (self::getData() as $key => $item) {
            $manager->persist($item);
            $this->addReference($key, $item);
        }

        $manager->flush();
        $manager->clear();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 1;
    }

    /**
     * @return array
     */
    private static function getDataFixture(): array
    {
        $loader = new NativeLoader();
        $objectSet = $loader->loadFile($_SERVER['PWD'] . '/config/fixtures/' . static::getFileNameFixture());
        return $dataFixture = $objectSet->getObjects();
    }

    /**
     * @return array
     */
    public static function getData(): array
    {
        if (!self::$data) {
            self::$data = self::getDataFixture();
        }

        return self::$data;
    }
}