<?php

namespace App\Controller;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractApiController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @return SerializerInterface
     */
    public function getSerializer(): SerializerInterface
    {
        if (!$this->serializer) {
            $this->serializer = $this->container->get('serializer');
        }

        return $this->serializer;
    }

    /**
     * @return array
     */
    public static function getSubscribedServices(): array
    {
        $subscribedServices = parent::getSubscribedServices();

        return array_merge($subscribedServices, ['serializer' => '?' . SerializerInterface::class]);
    }

    /**
     * @param mixed $data
     * @param int $status
     * @param array $headers
     * @param array|null $context
     * @return JsonResponse
     */
    protected function json($data, $status = 200, $headers = array(), $context = null): JsonResponse
    {
        if (isset($context)) {
            $context = null;
        } else {
            $context = SerializationContext::create();
        }

        if ($this->getSerializer()) {
            $data = json_decode($this->getSerializer()->serialize($data, 'json', $context), true);

        }

        return new JsonResponse([
            'data' => $data,
            'statusCode' => $status
        ], $status, $headers);
    }
}