<?php

namespace App\Controller;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api")
 *
 * Class ApiController
 * @package App\Controller
 */
class ApiController extends AbstractApiController
{
    /**
     * @Route("/articles")
     * @Method("GET")
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function articles(EntityManagerInterface $em): JsonResponse
    {
        $articles = $em->getRepository(Article::class)->findAll();

        return $this->json($articles);
    }

    /**
     * @Route("/articles")
     * @Method("POST")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function saveArticle(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $article = $this->getSerializer()->deserialize($request->getContent(), Article::class, 'json');

        $em->persist($article);
        $em->flush();

        return $this->json($article);
    }
}